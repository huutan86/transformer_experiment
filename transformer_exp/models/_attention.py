"""A module that define the architecture for the multi-head attention model."""
import math
from typing import Optional
from typing import Tuple
from typing import Union

import torch
import torch.nn as nn
import torch.nn.functional as F


class MultiheadAttention(nn.Module):
    """A class that implements the multi-head attention model.

    Args:
        input_dim: The input dimension of the MultiheadAttention. This is the model_dim, or the hidden dimension after
            the input is encoded,
        embeded_dim: The embedded dimension for each Q, K, V, normally assigned to be the same value as input_dim.
        num_heads: The number of attention heads.
    """

    _NUM_QKV_COMPONENTS = 3

    def __init__(self, input_dim: int, embeded_dim: int, num_heads: int) -> None:
        super().__init__()
        if embeded_dim % num_heads != 0:
            raise ValueError(f"#heads ({num_heads}) is not divisible by the embedded dimension ({embeded_dim})!")

        self._embeded_dim: int = embeded_dim
        self._num_heads: int = num_heads
        self._qkv_projector = nn.Linear(input_dim, self._NUM_QKV_COMPONENTS * embeded_dim)
        self._output_projector = nn.Linear(embeded_dim, embeded_dim)

    def forward(
        self, x: torch.Tensor, mask: Optional[torch.Tensor], return_attention_weights: bool = False
    ) -> Union[torch.Tensor, Tuple[torch.Tensor, torch.Tensor]]:
        """Computes the output of the multi-head attention.

        This function performs the following
        x --> QKV projector ---> Q => Head 0 attention calculation => [Attention concatenation] => [Projection]
                            |--> K
                            |--> V
                            (Head 0)

                            ---> Q
                            |--> K => Head 1 attention calculation
                            |--> V
                            (Head 1)
                            ....
        The dimension of x is [B, L, H], the total output dimension of QKV combined from all heads is
        [B, L, 3 * embeded_im]. After attention computation, the output will be []

        Args:
            x: The input tensor of shape [B, L, H] where H is the hidden dimension and L the sequence length, B is the
                batch size.

        Returns:
            - The output value of the multi-head attention block
            - The attention weights from all heads (if return_attention_weights == True).
        """
        batch_size, seq_len, _ = x.shape
        # This projection is in the encoding dimension.
        qkv = self._qkv_projector(x)

        # Split Q, K, V for each head.
        qkv = qkv.reshape(
            batch_size, seq_len, self._num_heads, self._NUM_QKV_COMPONENTS * self._embeded_dim // self._num_heads
        )  # [B, L, Head, QKV dim]
        qkv = qkv.permute(0, 2, 1, 3)  # [B, Head, L, QKV dim per head]

        q, k, v = qkv.chunk(chunks=self._NUM_QKV_COMPONENTS, dim=-1)  # Q, K, V each has shape of [B, Head, L, dk]
        attention_values, attention_weights = self._scale_dot_product(q=q, k=k, v=v, mask=mask)
        attention_values = attention_values.permute(0, 2, 1, 3)  # [B, L, Head, dk] where dk = self._embeded_dim/#heads
        attention_values = attention_values.reshape(batch_size, seq_len, self._embeded_dim)  # [B, L, embeded_dim]
        out = self._output_projector(attention_values)

        if return_attention_weights:
            return out, attention_weights
        return out

    @staticmethod
    def _scale_dot_product(
        q: torch.Tensor, k: torch.Tensor, v: torch.Tensor, mask: Optional[torch.Tensor]
    ) -> Tuple[torch.Tensor, torch.Tensor]:
        """Computes the Attention(Q, K, V) values.

        Attention(Q, K, V) = softmax(QK^T/ sqrt(dk)) * V
        dk is the per-head embedded dimension.

        Args:
            Q: The query tensor of shape [B, Head, L, dk].
            K: The key tensor of shape [B, Head, L, dk].
            V: The value tensor of shape [B, Head, L, dk].

        Returns:
            A tensor of Attention(Q, K, V) values of shape [B, Head, L, dk]
            A tensor of attention weights softmax(QK^T/ sqrt(dk)) [B, Head, L, L].

        """
        dk = q.size(-1)  # The encoding dimension. This is the hidden dimension of each key, value, and query.
        # Row i-th of the matrix QK^T is the attention between the element i of the sequence 0<= i< L to all other
        # element in the sequence.
        q_time_vt = torch.matmul(q, k.transpose(-2, -1))  # QK^T - shape [B, Head, L,  L]
        attention_logits = q_time_vt / math.sqrt(dk)
        if mask is not None:
            attention_logits = attention_logits.masked_fill(mask == 0, -9e15)
        attention_weights = F.softmax(
            attention_logits, dim=-1
        )  # Convert to softmax so that the attention is normalized.
        output_values = torch.matmul(attention_weights, v)  # [B, Head, L, dk]
        return output_values, attention_weights
