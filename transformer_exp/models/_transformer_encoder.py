"""A module that defines the encoder of the transformer."""
from enum import auto
from enum import Enum
from typing import List
from typing import Optional

import torch
import torch.nn as nn

from ._attention import MultiheadAttention


class LayerNormPos:
    """An enum class that defines the position of the Layer normalization layer."""

    POST = auto()
    PRE = auto()


class _EncoderBlockWithPostLayerNorm(nn.Module):
    """A class that defines the encoder block for the transformer encoder with layer normalization between res blocks.

    This class does the followings
    x------> [Multi-head attention]-> [Dropout] -> + -> [Norm] ---> [Linear layer] -> [Dropout] -> + -> [Norm] -> out
        |                                          ^            |                                  ^
        -------------------------------------------|            -----------------------------------|
    The skip connection avoid the vanishing gradient and make sure that the original signal and propagate forward to
        downstream blocks. Otherwise, the information is lost.

    Args:
        input_dim: This is the hidden dimension of the inputs.
        num_heads: The number of attention heads.
        feedforward_dim: The dimension of the layer in the feedforward network. Usually 2x or 4x larger than the input
            dimension.
        dropout_rate: The rate of the dropout in each encoder block.
    """

    def __init__(self, input_dim: int, num_heads: int, feedforward_dim: int, dropout_rate: float) -> None:
        super().__init__()
        self._self_attention = MultiheadAttention(input_dim=input_dim, embeded_dim=input_dim, num_heads=num_heads)
        self._drop_out = nn.Dropout(p=dropout_rate)
        self._layernorm1 = nn.LayerNorm(normalized_shape=input_dim)  # Normalize over the last dimension

        self._feedforward_net = nn.Sequential(
            nn.Linear(in_features=input_dim, out_features=feedforward_dim),
            nn.Dropout(p=dropout_rate),
            nn.ReLU(inplace=True),
            nn.Linear(in_features=feedforward_dim, out_features=input_dim),
        )
        self._layernorm2 = nn.LayerNorm(normalized_shape=input_dim)

    def forward(self, x, mask: Optional[torch.Tensor] = None) -> torch.Tensor:
        """Calculates the output of the encoding blocks.

        Args:
            x: The intput tensor of shape [B, L, input_shape]
        """
        attention_values = self._self_attention(x, mask=mask)  # The embedding dimension = input_dim
        x = x + self._drop_out(attention_values)
        x = self._layernorm1(x)  # [B, L, input_dim]

        # MLP part
        linear_out = self._feedforward_net(x)
        x = x + self._drop_out(linear_out)
        return self._layernorm2(x)


class _EncoderBlockWithPreLayerNorm(_EncoderBlockWithPostLayerNorm):
    """A class that defines the encoder block with layer normalization in the res connection.

    This architecture supports better gradient flows and removes the need for the warm-up stage.
    This class does the followings
    x------> [Norm] ---->[Multi-head attention]--> + ----------------[Norm]---> [Linear layer] --> + ---> out
        |                                          ^            |                                  ^
        -------------------------------------------|            -----------------------------------|
    The skip connection avoid the vanishing gradient and make sure that the original signal and propagate forward to
        downstream blocks. Otherwise, the information is lost.

    Args:
        input_dim: This is the hidden dimension of the inputs.
        num_heads: The number of attention heads.
        feedforward_dim: The dimension of the layer in the feedforward network. Usually 2x or 4x larger than the input
            dimension.
        dropout_rate: The rate of the dropout in each encoder block.

    Reference:
        http://proceedings.mlr.press/v119/xiong20b/xiong20b.pdf
    """

    def __init__(self, input_dim: int, num_heads: int, feedforward_dim: int, dropout_rate: float) -> None:
        super().__init__(
            input_dim=input_dim, num_heads=num_heads, feedforward_dim=feedforward_dim, dropout_rate=dropout_rate
        )

    def forward(self, x, mask: Optional[torch.Tensor] = None) -> torch.Tensor:
        """Calculates the output of the encoding blocks.

        Args:
            x: The intput tensor of shape [B, L, input_shape]
        """
        normed_x = self._layernorm1(x)
        x = x + self._self_attention(normed_x, mask=mask)
        normed_x2 = self._layernorm2(x)
        return x + self._feedforward_net(normed_x2)


class TransformerEncoder(nn.Module):
    """A class that defines the architecture for the transformer encoder by serializing many encoder blocks.

    This module contains a series of _EncoderBlocks. The input tensor will be passed through each of them. Once at a
        time.
    x -> [Enc block 1]  -> [Enc block 2] -> ....

    Args:
        num_encoder_blocks: The number of encoder blocks.
        input_dim: This is the hidden dimension of the inputs.
        num_heads: The number of attention heads.
        feedforward_dim: The dimension of the feedforward signal.
        dropout_rate: The rate of the dropout in each encoder block.
        layer_norm_pos (optional): The position of the layer normalization. Defaults to LayerNormPos.POST
    """

    def __init__(
        self,
        num_encoder_blocks: int,
        input_dim: int,
        num_heads: int,
        feedforward_dim: int,
        dropout_rate: float,
        layer_norm_pos: LayerNormPos = LayerNormPos.POST,
    ) -> None:
        super().__init__()
        if layer_norm_pos == LayerNormPos.POST:
            self._encoder_blocks = nn.ModuleList(
                [
                    _EncoderBlockWithPostLayerNorm(
                        input_dim=input_dim,
                        num_heads=num_heads,
                        feedforward_dim=feedforward_dim,
                        dropout_rate=dropout_rate,
                    )
                    for _ in range(num_encoder_blocks)
                ]
            )
        elif layer_norm_pos == LayerNormPos.PRE:
            self._encoder_blocks = nn.ModuleList(
                [
                    _EncoderBlockWithPreLayerNorm(
                        input_dim=input_dim,
                        num_heads=num_heads,
                        feedforward_dim=feedforward_dim,
                        dropout_rate=dropout_rate,
                    )
                    for _ in range(num_encoder_blocks)
                ]
            )
        else:
            raise ValueError(f"Layer normalization position of  {layer_norm_pos} is not supported!")

    def forward(self, x: torch.Tensor, mask: Optional[torch.Tensor] = None) -> torch.Tensor:
        """Calculates the output of the transformer encoders.

        Args:
            x: The input tensor of shape [B, L, H] where B is the batch size, L is the seq length, H is the hidden dim.
        """
        for b in self._encoder_blocks:
            x = b(x, mask=mask)
        return x

    def get_attention_maps(self, x, mask: Optional[torch.Tensor] = None) -> List[torch.Tensor]:
        """Returns the attention maps from multiple encoder blocks."""
        attention_maps: List[torch.Tensor] = []
        for b in self._encoder_blocks:
            attention_map = b._self_attention(x, mask=mask, return_attention_weights=True)[1]
            attention_maps.append(attention_map)
            # Make sure that the signal to the next block is generated.
            x = b(x, mask=mask)
        return attention_maps
