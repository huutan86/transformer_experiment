from typing import Any
from typing import Dict
from typing import List
from typing import Tuple

import torch
import torch.optim as optim
from torch.nn import functional as F

from ._scheduler import CosineWarmUpScheduler
from transformer_exp.models import Transformer
from transformer_exp.training_module._transformer_training_module import TransformerTrainingModule


class ReverseSequencePredictorTrainingModule(TransformerTrainingModule):
    """A training module to predict the reverse of a sequence.

    Args:
        input_dim: Hidden dimension of the input. For the sequence of numbers, this can be the number of values each
            item in the sequence can have.
        num_classes: The number of classes to predict per sequence element.
        embed_dim: The dimension of the embedding vector from the input vector.
        num_encoder_blocks: The number of encoder blocks in the transformer encoder.
        num_heads: The number of heads in the MultiHead Attention.
        lr: The learning rate of the model
        max_epochs: The maximum number of training epochs.
        num_warmup_epochs: The number of warm up epochs for the learning rate scheduler. After this steps, the learning
            rate will go down.
        input_dropout_rate (optional): The dropout rate to apply on input features. Defaults to 0.0
        transformer_encoder_dropout_rate (optional): The dropout rate inside the transformer encoder. Defaults to 0.0.
        add_positional_encoding (optional): If True (default), the positional encoding will be added to the input tensor
            after the first linear projection.
    """

    def __init__(
        self,
        input_dim: int,
        num_classes: int,
        embed_dim: int,
        num_encoder_blocks: int,
        num_heads: int,
        lr: float,
        max_epochs: int,
        num_warmup_epochs: int,
        input_dropout_rate: float = 0.0,
        transformer_encoder_dropout_rate: float = 0.0,
        add_positional_encoding: bool = True,
    ):
        super().__init__(lr=lr, max_epochs=max_epochs)
        self.num_classes: int = num_classes
        self._model = Transformer(
            input_dim=input_dim,
            embed_dim=embed_dim,
            num_encoder_blocks=num_encoder_blocks,
            num_heads=num_heads,
            num_classes=num_classes,
            input_dropout_rate=input_dropout_rate,
            transformer_encoder_dropout_rate=transformer_encoder_dropout_rate,
            add_positional_encoding=add_positional_encoding,
        )
        self._num_warmup_epochs: int = num_warmup_epochs

    def _calculate_loss(
        self, batch: Tuple[torch.Tensor, torch.Tensor], mode: str = "train"
    ) -> Tuple[torch.Tensor, torch.Tensor]:
        """Calculates the cross-entropy loss between the prediction and the ground truth and the accuracy."""
        source, target = batch
        source_one_hot = F.one_hot(source, num_classes=self.num_classes).float()

        # Perform prediction and calculate loss and accuracy
        # The order of the elements is important. That's why we need the positional encoding.
        predictions = self._model(source_one_hot, mask=None)  # [B, L, num_classes]
        target_view = target.view(-1)  # (N * L) that contains the class indices
        prediction_view = predictions.view(-1, predictions.size(-1))  # [N * L, C] with C be the number of classes.
        loss = F.cross_entropy(prediction_view, target_view)  # target
        predicted_target = predictions.argmax(dim=-1)  # [N, L]
        acc = (predicted_target == target).float().mean()

        # Logging.
        self.log(f"{mode}_loss", loss)
        self.log(f"{mode}_acc", acc)
        return loss, acc

    def configure_optimizers(self) -> Tuple[torch.optim.Optimizer, List[Dict[str, Any]]]:
        """Configures the optimizer."""
        optimizer = optim.Adam(self.parameters(), lr=self._lr)
        lr_scheduler = CosineWarmUpScheduler(
            optimizer=optimizer, num_warmup_epochs=self._num_warmup_epochs, max_epochs=self._max_epochs
        )
        return [optimizer], [{"scheduler": lr_scheduler, "interval": "epoch"}]

    def get_attention_maps(self, x: torch.Tensor) -> torch.Tensor:
        return self._model.get_attention_maps(x)
