"""A module for splitting the images into small patches."""
import torch


def image_to_patch(x: torch.Tensor, patch_size: int, flatten_channel: bool = True) -> torch.Tensor:
    """Split an image tensor of shape [B, C, H, W] into [B, #patches, C * p_H * p_W] or [B, #patches, C, p_H, p_W].

    Args:
        x: The input image of shape [B, C, H, W].
        patch_size: The size of the patch
        flatten_channel (optional): If True, the patch data will be flattened (default) to generate the output shape of
            [B, #patches, C * p_H * p_W]. Otherwise, the output tensor will be [B, #patches, C, p_H, p_W]
    """
    B, C, H, W = x.shape
    x = x.reshape(B, C, H // patch_size, patch_size, W // patch_size, patch_size)
    x = x.permute(0, 2, 4, 1, 3, 5)  # B, #patch_h, #patch_w, C, p_H, p_W
    x = x.flatten(start_dim=1, end_dim=2)  # B, #patches, C, p_H, p_W
    if flatten_channel:
        x = x.flatten(start_dim=2, end_dim=4)  # B, #patches, C * p_H * p_W
    return x
