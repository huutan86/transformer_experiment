"""A module that defines the transformer architecture."""
from typing import List
from typing import Optional

import torch
import torch.nn as nn

from ._positional_encoder import PositionalEncoding
from ._transformer_encoder import TransformerEncoder


class Transformer(nn.Module):
    """A class that defines the whole transformer architecture.

    Args:
        input_dim: The dimension of the input.
        embed_dim: The dimension of the embedding vector from the input vector.
        num_encoder_blocks: The number of encoder blocks in the transformer encoder.
        num_heads: The number of heads in the MultiHead Attention.
        num_classes: The number of classes to predict per sequence element.
        transformer_encoder_dropout_rate (optional): The dropout rate inside the transformer encoder. Defaults to 0.0.
        add_positional_encoding (optional): If True (default), the positional encoding will be added to the input tensor
            after the first linear projection.
        input_dropout_rate (optional): The dropout rate to apply on input features. Defaults to 0.0
    """

    def __init__(
        self,
        input_dim: int,
        embed_dim: int,
        num_encoder_blocks: int,
        num_heads: int,
        num_classes: int,
        input_dropout_rate: float = 0.0,
        transformer_encoder_dropout_rate: float = 0.0,
        add_positional_encoding: bool = True,
    ) -> None:
        super().__init__()
        self._input_embedding_network: nn.Module = nn.Sequential(
            nn.Dropout(p=input_dropout_rate), nn.Linear(in_features=input_dim, out_features=embed_dim)
        )
        self._add_positional_encoding = add_positional_encoding
        self._positional_encoder = PositionalEncoding(hidden_dim=embed_dim)
        self._transformer_encoder = TransformerEncoder(
            num_encoder_blocks=num_encoder_blocks,
            input_dim=embed_dim,  # Input dimension of the transformer encoder = hidden embedded dimension.
            feedforward_dim=2 * embed_dim,  # Normally 2x-4x bigger than the input dimension.
            num_heads=num_heads,
            dropout_rate=transformer_encoder_dropout_rate,
        )

        # Output classifier per sequence element
        self._classification_net = nn.Sequential(
            nn.Linear(in_features=embed_dim, out_features=embed_dim),
            nn.LayerNorm(normalized_shape=embed_dim),
            nn.ReLU(inplace=True),
            nn.Dropout(transformer_encoder_dropout_rate),
            nn.Linear(embed_dim, num_classes),
        )

    def forward(self, x: torch.Tensor, mask: Optional[torch.Tensor] = None) -> torch.Tensor:
        """Calculates the output tensor from the input tensor.

        The order of the operation will be
        x => Input Network => Add positional Encoding Network => Transformer => Output network => Output

        Args:
            x: The input tensor.
        """
        x = self._calculate_transformer_encoder_input(x)
        x = self._transformer_encoder(x, mask=mask)  # [B, L, model_dim]
        return self._classification_net(x)  # [B, L, num_classes]

    def _calculate_transformer_encoder_input(self, x: torch.Tensor) -> torch.Tensor:
        x = self._input_embedding_network(x)
        if self._add_positional_encoding:
            x = self._positional_encoder(x)
        return x

    @torch.no_grad()
    def get_attention_maps(self, x: torch.Tensor) -> List[torch.Tensor]:
        """Gets the attention maps for the tensor.

        Args:
            x: The input tensor to compute the attention maps.
        """
        x = self._calculate_transformer_encoder_input(x)
        return self._transformer_encoder.get_attention_maps(x)
