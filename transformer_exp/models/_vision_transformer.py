from typing import Optional

import torch
import torch.nn as nn

from transformer_exp.image_proc import image_to_patch
from transformer_exp.models import LayerNormPos
from transformer_exp.models import TransformerEncoder


class VisionTransformer(nn.Module):
    """A class that defines the vision transformer architecture.

    Args:
        num_input_channels: The number of input channels.
        patch_size_pixels: The size of each patches in pixels
        embed_dim: The dimension of the embedding vector from the input vector.
        num_encoder_blocks: The number of encoder blocks in the transformer encoder.
        num_heads: The number of heads in the MultiHead Attention.
        num_classes: The number of classes to predict per sequence element.
        feedforward_dim: The dimension of the feedforward layer in each encoder block.
        num_patches: The number of patches that each image can have.
        transformer_encoder_dropout_rate (optional): The dropout rate inside the transformer encoder. Defaults to 0.0.
    """

    def __init__(
        self,
        num_input_channels: int,
        patch_size_pixels: int,
        embed_dim: int,
        num_encoder_blocks: int,
        num_heads: int,
        num_classes: int,
        feedforward_dim: int,
        num_patches: int,
        transformer_encoder_dropout_rate: float = 0.0,
    ) -> None:
        super().__init__()
        # Input layer, performs Drop out and dimension conversion.
        # input shape = [B, #patches, (patch_size ** 2) * num_channels] where B is the batch size,
        self._patch_size_pixels = patch_size_pixels
        self._input_embedding_network: nn.Module = nn.Linear(
            in_features=num_input_channels * (patch_size_pixels**2), out_features=embed_dim
        )
        self._drop_out = nn.Dropout(transformer_encoder_dropout_rate)

        self._transformer_encoder = TransformerEncoder(
            num_encoder_blocks=num_encoder_blocks,
            input_dim=embed_dim,
            num_heads=num_heads,
            feedforward_dim=feedforward_dim,
            dropout_rate=transformer_encoder_dropout_rate,
            layer_norm_pos=LayerNormPos.PRE,
        )
        self._classification_head = nn.Sequential(
            # Note a simpler architecture than the normal transformer architecture.
            nn.LayerNorm(normalized_shape=embed_dim),
            nn.Linear(in_features=embed_dim, out_features=num_classes),
        )

        self._class_token = nn.Parameter(torch.randn(1, 1, embed_dim))
        # A learnable embedding prepended to the sequence of embeded patches.
        # We will use the output feature vector of the classification token (CLS token in short) for determining the
        # classification prediction.

        self._positional_embeddings = nn.Parameter(
            torch.randn(1, 1 + num_patches, embed_dim)
        )  # +1 for the class embedding.

    def forward(self, x: torch.Tensor, mask: Optional[torch.Tensor] = None) -> torch.Tensor:
        """Calculates the output tensor.

        The order of the operation will be
        x => Input Network => Append =>  Add positional Encoding Network => Dropout => Transformer => y[0], ... y[ptcs]
                                ^^
                                ||
                                ||
                          [class token]

        y[0]  => classification head => classification result
        Args:

            x: The input tensor of size [B, C, H, W].

        Outputs:
            A tensor of [B, #cls]
        """
        patches = image_to_patch(
            x, patch_size=self._patch_size_pixels, flatten_channel=True
        )  # [B, #pts, C * p_H * p_W]
        x = self._input_embedding_network(patches)  # [B, #patches, embed_dim]
        batch_size, num_patches = patches.size(0), patches.size(1)

        # Add class token for all patches.
        class_token = self._class_token.repeat(batch_size, 1, 1)  # [B, #patches, embeddim]
        x = torch.cat([class_token, x], dim=1)  # [B, #patches + 1, embed_dim]
        x += self._positional_embeddings[:, : num_patches + 1]

        x = self._drop_out(x)  # [B, #patches + 1, embeded_dim]
        x = self._transformer_encoder(x)  # [B, #patches + 1, embeded dim]

        # Class prediction using the class token of the last attention block.
        return self._classification_head(x[:, 0])
