"""Transformer training module definition."""
import logging
from abc import abstractmethod
from typing import Any
from typing import Dict
from typing import List
from typing import Tuple

import pytorch_lightning as pl
import torch
import torch.optim as optim


class TransformerTrainingModule(pl.LightningModule):
    """A lightning-based training module.

    Args:
        lr: The learning rate of the model
        max_epochs: The maximum number of training epochs.
    """

    def __init__(self, lr: float) -> None:
        super().__init__()
        self._lr: float = lr

    @abstractmethod
    def configure_optimizers(self) -> Tuple[torch.optim.Optimizer, List[Dict[str, Any]]]:
        raise NotImplementedError

    def training_step(self, batch: torch.Tensor, batch_idx: int) -> Tuple[torch.Tensor, torch.Tensor]:
        """Training step."""
        return self._calculate_loss(batch, mode="train")  # Returns the loss.

    def training_step_end(
        self, workers_outputs: Tuple[List[torch.Tensor], List[torch.Tensor]]
    ) -> Dict[str, torch.Tensor]:
        """Aggregates the results from all workers."""
        return {"loss": workers_outputs[0].mean(), "acc": workers_outputs[1].mean()}

    def training_epoch_end(self, batch_outputs: List[Dict[str, torch.Tensor]]) -> None:
        """Combines the loss from all workers."""
        mean_loss = torch.stack([b["loss"] for b in batch_outputs]).mean()
        mean_acc = torch.stack([b["acc"] for b in batch_outputs]).mean()
        logging.info(f"Train loss = {mean_loss:0.2f}, acc = {mean_acc:0.3f}")
        self.log(f"train_loss", mean_loss)
        self.log(f"train_acc", mean_acc)

    def validation_step(self, batch: torch.Tensor, batch_idx: int) -> Tuple[torch.Tensor, torch.Tensor]:
        """Validation step."""
        return self._calculate_loss(batch, mode="val")

    def validation_step_end(
        self, workers_outputs: Tuple[List[torch.Tensor], List[torch.Tensor]]
    ) -> Dict[str, torch.Tensor]:
        """Aggregates the results from all workers."""
        return {"loss": workers_outputs[0].mean(), "acc": workers_outputs[1].mean()}

    def validation_epoch_end(self, batch_outputs: List[Dict[str, torch.Tensor]]) -> None:
        """Combines the loss from all workers."""
        mean_loss = torch.stack([b["loss"] for b in batch_outputs]).mean()
        mean_acc = torch.stack([b["acc"] for b in batch_outputs]).mean()
        logging.info(f"Validation loss = {mean_loss:0.2f}, acc = {mean_acc:0.3f}")
        self.log(f"val_loss", mean_loss)
        self.log(f"val_acc", mean_acc)

    def test_step(self, batch: torch.Tensor, batch_idx: int) -> Tuple[torch.Tensor, torch.Tensor]:
        """Test step."""
        return self._calculate_loss(batch, mode="test")

    def test_step_end(self, workers_outputs: Tuple[List[torch.Tensor], List[torch.Tensor]]) -> Dict[str, torch.Tensor]:
        """Aggregates the results from all workers."""
        return {"loss": workers_outputs[0].mean(), "acc": workers_outputs[1].mean()}

    def test_epoch_end(self, batch_outputs: List[Dict[str, torch.Tensor]]) -> None:
        """Combines the loss from all workers."""
        mean_loss = torch.stack([b["loss"] for b in batch_outputs]).mean()
        mean_acc = torch.stack([b["acc"] for b in batch_outputs]).mean()
        logging.info(f"Test loss = {mean_loss:0.2f}, acc = {mean_acc:0.3f}")
        self.log(f"test_loss", mean_loss)
        self.log(f"test_acc", mean_acc)

    @abstractmethod
    def _calculate_loss(
        self, batch: Tuple[torch.Tensor, torch.Tensor], mode: str = "train"
    ) -> Tuple[torch.Tensor, torch.Tensor]:
        raise NotImplementedError
