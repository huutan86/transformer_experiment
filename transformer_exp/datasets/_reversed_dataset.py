"""A dataset that provides reverse data."""
from typing import Tuple

import torch
from torch.utils.data import Dataset


class ReverseDataset(Dataset):
    """A dataset that provides reverse data.

    Args:
        num_categories: The number of categories. This is the maximum value (excluded) of all entries in the sequence.
        seq_len: The length of the sequence.
        size: The size of the dataset
    """

    def __init__(self, num_categories: int, seq_len: int, size: int) -> None:
        super().__init__()
        self._size = size
        self._data: torch.Tensor = torch.randint(low=0, high=num_categories, size=(self._size, seq_len))

    def __len__(self) -> int:
        return self._size

    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, torch.Tensor]:
        raw_data = self._data[idx]
        return raw_data, torch.flip(raw_data, dims=(0,))
