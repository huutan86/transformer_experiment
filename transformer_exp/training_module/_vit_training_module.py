"""A training module for the vision transformer."""
from typing import Any
from typing import Dict
from typing import List
from typing import Tuple

import torch
import torch.nn.functional as F
import torch.optim as optim

from transformer_exp.models import VisionTransformer
from transformer_exp.training_module._transformer_training_module import TransformerTrainingModule


class ViTTrainingModule(TransformerTrainingModule):
    """A training module for the Vision Transformer.

    Args:
        lr: The learning rate of the model

        num_input_channels: The number of the input channels (C). 3 for RGB images.
        patch_size_pixels: The size of each patch.
        embed_dim: The embedding dimension from the input vector.
        num_encoder_blocks: The number of encoder blocks in the transformer encoder.
        num_heads: The number of heads in the MultiHead Attention.
        num_classes: The number of classes to predict.
        feedforward_dim: The dimension of the feedforward network.
        num_patches: The number of patches that each image can have.
        transformer_encoder_dropout_rate (optional): The dropout rate inside the transformer encoder. Defaults to 0.0.
    """

    def __init__(
        self,
        lr: float,
        num_input_channels: int,
        patch_size_pixels: int,
        embed_dim: int,
        num_encoder_blocks: int,
        num_heads: int,
        num_classes: int,
        feedforward_dim: int,
        num_patches: int,
        transformer_encoder_dropout_rate: float = 0.0,
    ):
        super().__init__(lr=lr)
        self._model = VisionTransformer(
            num_input_channels=num_input_channels,
            patch_size_pixels=patch_size_pixels,
            embed_dim=embed_dim,
            num_encoder_blocks=num_encoder_blocks,
            num_heads=num_heads,
            num_classes=num_classes,
            feedforward_dim=feedforward_dim,
            num_patches=num_patches,
            transformer_encoder_dropout_rate=transformer_encoder_dropout_rate,
        )

    def configure_optimizers(self) -> Tuple[torch.optim.Optimizer, List[Dict[str, Any]]]:
        """Configures the optimizer."""
        optimizer = optim.AdamW(self.parameters(), lr=self._lr)
        lr_scheduler = optim.lr_scheduler.MultiStepLR(
            optimizer=optimizer, milestones=[200, 1000], gamma=0.1, verbose=True
        )  # Because we use Pre-LN Transformer Encoder, warmup
        # is not needed.
        return [optimizer], [lr_scheduler]

    def _calculate_loss(
        self, batch: Tuple[torch.Tensor, torch.Tensor], mode: str = "train"
    ) -> Tuple[torch.Tensor, torch.Tensor]:
        """Calculates the cross-entropy loss between the prediction and the ground truth and the accuracy.

        Args:
            batch: A tuple of source, label image. Source has the shape of [B, C, H, W]
                Label has the shape of [B, 1] containing the label of each image.
        """
        sources, labels = batch
        logits = self._model(x=sources, mask=None)
        loss = F.cross_entropy(logits, labels)
        acc = (logits.argmax(dim=-1) == labels).float().mean()
        return loss, acc
