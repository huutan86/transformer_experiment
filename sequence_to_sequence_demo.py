"""Demo script for sequence to sequence conversion using transformer."""
import logging
import os

import matplotlib.pyplot as plt

from _utils import _get_training_devices

logging.getLogger().setLevel(logging.DEBUG)
from transformer_exp.datasets import ReverseDataset
from functools import partial
from torch.utils.data import DataLoader
import pytorch_lightning as pl
import torch.nn as nn
import torch.nn.functional as F
from pytorch_lightning.callbacks import ModelCheckpoint
from transformer_exp.training_module import ReverseSequencePredictorTrainingModule
import torch
from pytorch_lightning.plugins import DDPPlugin

_CHECKPOINT_PATH = "~/Documents/transformer_exp"


def _train_model(
    train_dataloader: DataLoader, val_dataloader: DataLoader, test_dataloader: DataLoader, **training_kws
) -> None:
    """Trains the model."""
    root_dir = os.path.join(_CHECKPOINT_PATH, "ReverseSequenceModel")
    os.makedirs(root_dir, exist_ok=True)
    trainer = pl.Trainer(
        default_root_dir=root_dir,
        callbacks=[ModelCheckpoint(save_weights_only=True, mode="max", monitor="val_acc")],
        accelerator=_get_training_devices(),
        devices=1 if _get_training_devices() == "cpu" else -1,
        max_epochs=15,
        gradient_clip_val=5,
    )
    trainer.logger._default_hp_metric = None  # No need optional logging arguments.
    model = ReverseSequencePredictorTrainingModule(max_epochs=trainer.max_epochs, **training_kws)
    trainer.fit(model=model, train_dataloaders=train_dataloader, val_dataloaders=val_dataloader)

    _visualize_attention_map(model=model, val_dataloader=val_dataloader)

    # Visualize the attention heatmaps.
    # Test the model on the validation and the test st
    val_result = trainer.test(model, val_dataloader, verbose=False)
    test_result = trainer.test(model, test_dataloader, verbose=False)
    print(f"Val acc: {100.0 * val_result[0]['test_acc']:.3f}, test_acc: {100 * test_result[0]['test_acc']:.3f} ")


def _visualize_attention_map(model: nn.Module, val_dataloader: DataLoader):
    source, _ = next(iter(val_dataloader))
    source_one_hot = F.one_hot(source, num_classes=model.num_classes).float()
    source_one_hot = source_one_hot.to(torch.device("cpu"))
    attention_maps = model.get_attention_maps(source_one_hot)  # A list of [B, #heads, L, L] attention maps.
    num_heads = attention_maps[0].shape[1]
    seq_len = source.shape[1]
    num_encoder_blocks = len(attention_maps)

    # Create a 2D plot where each row is for 1 encoder block, each column is for 1 head.

    fig_size = 3
    fig, ax = plt.subplots(num_encoder_blocks, num_heads, figsize=(num_encoder_blocks * fig_size, num_heads * fig_size))
    sample_idx = 0

    # Matplotlib treats the case of 1x1 subplot differently.
    if num_encoder_blocks == 1:
        ax = [ax]
    if num_heads == 1:
        ax = [[a] for a in ax]

    for r in range(num_encoder_blocks):
        for c in range(num_heads):
            ax[r][c].imshow(attention_maps[r][sample_idx, c], origin="lower", vmin=0, vmax=0.2)
            ax[r][c].set_xticks(list(range(seq_len)))
            ax[r][c].set_xticklabels(source[sample_idx].tolist())
            ax[r][c].set_xticks(list(range(seq_len)))
            ax[r][c].set_yticklabels(source[sample_idx].tolist())
            ax[r][c].set_title(f"Attention map block{r} head {c}")
    fig.subplots_adjust(hspace=0.5)
    plt.show()
    print("Done!")


def _main():
    """Do the main training script."""
    logging.info("Training the transformer")
    NUM_CATEGORIES = 10
    SEQ_LEN = 16
    dataset_factory = partial(ReverseDataset, NUM_CATEGORIES, SEQ_LEN)
    train_dataloader = DataLoader(dataset_factory(50000), batch_size=128, shuffle=True, drop_last=True, pin_memory=True)
    val_dataloader = DataLoader(dataset_factory(2000), batch_size=128)
    test_dataloader = DataLoader(dataset_factory(10000), batch_size=128)

    # Print out an example from the dataset
    logging.debug("An example from the training dataset")
    source, target = train_dataloader.dataset[0]
    logging.debug(f"source: {source}, target: {target}")
    training_module_kwargs = {
        "input_dim": NUM_CATEGORIES,
        "lr": 5e-4,
        "num_classes": NUM_CATEGORIES,
        "embed_dim": 32,
        "num_encoder_blocks": 2,  # The number of blocks inside the transformer encoder.
        "num_heads": 1,
        "num_warmup_epochs": 10,
        "add_positional_encoding": True,
    }
    _train_model(
        train_dataloader=train_dataloader,
        val_dataloader=val_dataloader,
        test_dataloader=test_dataloader,
        **training_module_kwargs,
    )


if __name__ == "__main__":
    _main()
