"""A module that defines the scheduler."""
from typing import List

import numpy as np
import torch.optim as optim


class CosineWarmUpScheduler(optim.lr_scheduler._LRScheduler):
    """A class that defines the cosine warmup scheduling.

    This scheduler allow us to gradually increase the learning rate from 0 on to our originally specified learning rate
    in the first few iterations. Thus, we slowly start learning instead of taking very large steps from the beginning.
    In fact, training a deep Transformer without learning rate warm-up can make the model diverge and achieve a much
    worse performance on training and testing. Take for instance the following plot by [Liu et al. (2019])(
    https://arxiv.org/pdf/1908.03265.pdf) comparing
    Adam-vanilla (i.e. Adam without warm-up) vs Adam with a warm-up:

    There are currently two common explanations. Firstly, Adam uses the bias correction factors which however can lead
    to a higher variance in the adaptive learning rate during the first iterations. Improved optimizers like RAdam have
    been shown to overcome this issue, not requiring warm-up for training Transformers. Secondly, the iteratively
    applied Layer Normalization across layers can lead to very high gradients during the first iterations, which can be
    solved by using Pre-Layer Normalization (similar to Pre-Activation ResNet), or replacing Layer Normalization by
    other techniques (Adaptive Normalization, Power Normalization).

    Nevertheless, many applications and papers still use the original Transformer architecture with Adam, because
    warm-up is a simple, yet effective way of solving the gradient problem in the first iterations. There are many
    different schedulers we could use. For instance, the original Transformer paper used an exponential decay scheduler
    with a warm-up. However, the currently most popular scheduler is the cosine warm-up scheduler, which combines
    warm-up with a cosine-shaped learning rate decay. We can implement it below, and visualize the learning rate factor
    over epochs.

    Args:
        optimizer: The optimizer to schedule.
        num_warmup_epochs: The number of linear warmup epochs. During this phase, the warmup will be almost linear to
            the epoch index. After this phase, the lr_factor will be gradually decreasing.
        max_epochs: The maximum number of epochs The learning rate will be reduced to 0 and hit 0 at this step.
    """

    def __init__(self, optimizer: optim.Optimizer, num_warmup_epochs: int, max_epochs: int):
        self._num_warmup_epochs = num_warmup_epochs
        self.max_iters = max_epochs
        super().__init__(optimizer)

    def get_lr(self) -> List[float]:
        """Gets the learning rate."""
        lr_factor = self._get_lr_factor(epoch=self.last_epoch)
        return [base_lr * lr_factor for base_lr in self.base_lrs]

    def _get_lr_factor(self, epoch: int) -> float:
        lr_factor = 0.5 * (1 + np.cos(np.pi * epoch / self.max_iters))
        if epoch <= self._num_warmup_epochs:
            lr_factor *= float(epoch) / self._num_warmup_epochs
        return lr_factor
