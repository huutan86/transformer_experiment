"""A modules that defines the positional encoders."""
import math

import matplotlib.pyplot as plt
import seaborn as sns
import torch
import torch.nn as nn


class PositionalEncoding(nn.Module):
    """A class that defines the positional encoding for each element in the sequence.

    Args:
        hidden_dim: The hidden dimension of each element in the sequence. This is the dimension that we use to encode
            the position. Each position in the sequence will be encoded with different encoding vectors of the length
            hidden_dim.
        max_len (optional): The maximum length of the sequence to expect. This can be bigger than the length of the
            sequence.
    """

    def __init__(self, hidden_dim: int, max_len: int = 5000) -> None:
        super().__init__()
        pos_enc = torch.zeros(max_len, hidden_dim)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)  # Position of the elements in the sequence.
        # The positional encoding for the element (pos, i) where pos is the position in the sequence and i is the pos
        # in the hidden dimension is given by
        # v = sin{pos / 10000 ^ [i / hidden_dim]} if i is even for i in range [0, hidden_dim], step = 2
        # and cos{pos / 10000 ^ [(i-1) / hidden_dim] if i is odd}
        # If we visualize the position encoding as a 2D array when each row is for 1 position, and each column is for 1
        # hidden dimension. We will have a sine/cosine signal for a fix value of i.
        # - When i = 0, we have a perfect sin, cos function
        # When we fix the value of pos, we will have a chirp for different i values where the freuency increases w.r.t i
        # Therefore, we characterize each position by a chirp signal.
        # Note the division term = 10000 ^ (a/b) = exp(a * log10000 / b).

        div_term = torch.exp(torch.arange(0, hidden_dim, 2).float() * (math.log(10000.0) / hidden_dim))
        pos_enc[:, 0::2] = torch.sin(position / div_term)
        pos_enc[:, 1::2] = torch.cos(position / div_term)
        pos_enc = pos_enc.unsqueeze(0)  # Add minibatch size dimension.

        # Register the positional encoding as a part of the module state.
        # persistent = False means do not add the positional encoding to ths state dict.
        self.register_buffer("pe", pos_enc, persistent=False)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        seq_len = x.size(1)
        x = x + self.pe[:, :seq_len]
        return x


def _visualize_positional_encodings() -> None:
    """A function to visualize the positional encodings."""
    positional_encoder = PositionalEncoding(hidden_dim=48, max_len=96)
    pe_mat = positional_encoder.pe.squeeze().cpu().numpy()
    plt.figure(1)
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(8, 3))
    ax.imshow(pe_mat, cmap="RdGy", extent=(1, pe_mat.shape[1] + 1, pe_mat.shape[0] + 1, 1))
    ax.set_xlabel("Hidden dimension")
    ax.set_ylabel("Position in sequence")

    # Plot the encodings of different positions
    plt.figure()
    pos_to_draw = [1, 5, 8]
    for p in pos_to_draw:
        plt.plot(pe_mat[p, :20], color=f"C{p}")

    plt.xlabel("Hidden dimensions")
    plt.ylabel("Values of the encodins")
    plt.title(f"Encoding vectors for 3 different positions {pos_to_draw}")
    plt.show()


if __name__ == "__main__":
    _visualize_positional_encodings()
