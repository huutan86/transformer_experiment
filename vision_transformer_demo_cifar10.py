"""Demo script for train a vision transformer ViT-B/16 on CIFAR10."""
import logging
import os
from typing import List

import matplotlib.pyplot as plt
import torch

from _utils import _get_training_devices

logging.getLogger().setLevel(logging.DEBUG)
from torch.utils.data import DataLoader
import pytorch_lightning as pl
import torch.nn as nn
from torchvision import transforms
from torchvision.datasets import CIFAR10
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
import torchvision
from typing import Union
from transformer_exp.image_proc import image_to_patch
from transformer_exp.training_module import ViTTrainingModule

if _get_training_devices() == "cpu":  # Data running on Mac
    _CHECKPOINT_PATH = "~/Documents/transformer_exp"
elif _get_training_devices() == "gpu":  # Running on the Pod
    _CHECKPOINT_PATH = "/jupyter-users-home/tan-2enguyen/datasets/transformer_exp"

_FULL_IMAGE_SIZE = 32
_PATCH_SIZE = 8  #
_BATCH_SIZE = 8192
_NUM_DATALOADING_WORKERS = 8


def _main():
    """Do the main training script."""
    logging.info("Training the vision transformer for CIFAR10")
    DATASET_PATH = "./transformer_exp/datasets/data"
    normalize_transform = transforms.Normalize(
        mean=[0.49139968, 0.48215841, 0.44653091], std=[0.24703223, 0.24348513, 0.26158784]
    )
    test_transform = transforms.Compose([transforms.ToTensor(), normalize_transform])
    pl.seed_everything(42)

    # Generate 45000 training images and 5000 validation images.
    train_set = CIFAR10(
        root=DATASET_PATH,
        train=True,
        transform=transforms.Compose(
            [
                transforms.RandomHorizontalFlip(),
                transforms.RandomVerticalFlip(),
                transforms.RandomResizedCrop(
                    size=(_FULL_IMAGE_SIZE, _FULL_IMAGE_SIZE)
                ),  # Crop and resize it to this size,
                transforms.ToTensor(),
                normalize_transform,
            ]
        ),
        download=False,
    )

    pl.seed_everything(42)
    val_set = CIFAR10(root=DATASET_PATH, train=False, transform=test_transform, download=False)

    train_dataloader = DataLoader(
        train_set,
        batch_size=_BATCH_SIZE,
        shuffle=True,
        drop_last=True,  # drop the last incomplete patch.
        pin_memory=True,
        num_workers=_NUM_DATALOADING_WORKERS,
        persistent_workers=True,
    )

    val_dataloader = DataLoader(
        val_set,
        batch_size=_BATCH_SIZE,
        shuffle=False,
        drop_last=False,
        num_workers=_NUM_DATALOADING_WORKERS,
        persistent_workers=True,
        pin_memory=True,
    )

    logging.info(f"Sample 0th dimension {val_set[0][0].shape}, label = {val_set[0][1]}")
    _train_model(train_dataloader=train_dataloader, val_dataloader=val_dataloader)

    # _NUM_IMGS_TO_VISUALIZE = 4
    # ims_to_display = torch.stack([val_set[i][0] for i in range(_NUM_IMGS_TO_VISUALIZE)], dim=0)
    # ims_batches = image_to_patch(ims_to_display, patch_size=16, flatten_channel=False)
    # _visualize_batch_images(ims=ims_batches[1])  # Display all the patches from the 1st image.


def _train_model(train_dataloader: DataLoader, val_dataloader: DataLoader) -> None:
    root_dir = os.path.join(_CHECKPOINT_PATH, "ViT")
    os.makedirs(root_dir, exist_ok=True)
    trainer = pl.Trainer(
        default_root_dir=root_dir,
        callbacks=[
            ModelCheckpoint(save_weights_only=True, mode="max", monitor="val_acc"),
            LearningRateMonitor("epoch"),
        ],
        accelerator=_get_training_devices(),
        devices=1 if _get_training_devices() == "cpu" else -1,
        max_epochs=10000,
        check_val_every_n_epoch=5,
        strategy="dp",
        enable_progress_bar=True,
        profiler=None,
    )
    model = ViTTrainingModule(
        lr=3e-4,
        num_input_channels=3,
        patch_size_pixels=_PATCH_SIZE,
        embed_dim=256,
        num_encoder_blocks=8,
        num_heads=8,
        num_classes=10,
        num_patches=(_FULL_IMAGE_SIZE // _PATCH_SIZE) ** 2,
        feedforward_dim=1024,  # 2-4x the input dimension of the Encoder network.
    )
    trainer.fit(model=model, train_dataloaders=train_dataloader, val_dataloaders=val_dataloader)


def _visualize_batch_images(ims: Union[List[torch.Tensor], torch.Tensor]) -> None:
    """Visualizes the images.

    Args:
        ims: A list of images to visualize. Each has dimensions of [C, H, W].
    """
    if isinstance(ims, list):
        ims = torch.stack(ims, dim=0)
    im_grid = torchvision.utils.make_grid(ims, nrow=ims.size(0), normalize=True, pad_value=0.9).permute(1, 2, 0)
    plt.figure()
    plt.imshow(im_grid)
    plt.axis("off")
    plt.show()


if __name__ == "__main__":
    _main()
