import logging

import torch


def _get_training_devices() -> str:
    """Gets the training devices."""
    device = "gpu" if torch.cuda.is_available() else "cpu"
    logging.info(f"Device available {device}")
    return device
